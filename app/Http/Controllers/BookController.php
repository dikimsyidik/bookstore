<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Tag;
use App\Http\Requests\BookRequest;

   /**
     * @OA\Info(title="My First API", version="0.1")
     */
class BookController extends Controller
{


    public function index(Request $request)
    {
        $query = Book::with('tags');
        if ($request->title != '') {
            $query->where('title', 'ILIKE', '%' . $request->input('title') . '%');
        }
        if ($request->writer != '') {
            $query->where('writer', 'ILIKE', '%' . $request->input('writer') . '%');
        }

        $books = $query->paginate(5);

        return response()->json($books);
    }
    public function store(BookRequest $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'writer' => 'required|string',
            'cover_image' => 'required|url',
            'price' => 'required|numeric',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
        ]);

        $book = Book::create($validatedData);
        if ($request->has('tags')) {
            $tags = [];
            foreach ($request->tags as $tag) {
                $tags[] = Tag::firstOrCreate(['name' => $tag])->id;
            }
            $book->tags()->attach($tags);
        }

        return response()->json(['message' => 'Book created successfully', 'book' => $book], 201);
    }

    public function update(BookRequest $request, Book $book)
    {
        $validatedData = $request->validate([
            'title' => 'string',
            'writer' => 'string',
            'cover_image' => 'url',
            'price' => 'numeric',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
        ]);

        $book->update($validatedData);

        if ($request->has('tags')) {
            $tags = [];
            foreach ($request->tags as $tag) {
                $tags[] = Tag::firstOrCreate(['name' => $tag])->id;
            }
            $book->tags()->sync($tags);
        }

        return response()->json(['message' => 'Book updated successfully', 'book' => $book]);
    }

    public function destroy(Book $book)
    {
        $book->tags()->detach();
        $book->delete();

        return response()->json(['message' => 'Book deleted successfully']);
    }



}
