<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\User;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\OrderRequest;

class OrderController extends Controller
{

    public function index()
    {
        $user = auth('api')->user();;


        $orders = Order::where('user_id',$user->id)->paginate(10);
        return response()->json($orders);
    }
      public function order(OrderRequest $request)


        {
            // Validate request data
            $validator = Validator::make($request->all(), [
                'book_id' => 'required|exists:books,id',
                'quantity' => 'required|integer|min:1',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 400);
            }

            // Fetch the book
            $book = Book::find($request->input('book_id'));

            // Calculate total price
            $totalPrice = $book->price * $request->input('quantity');

            // Check if customer has enough points
            $customer = auth('api')->user();
            if ($customer->points < $totalPrice) {
                return response()->json(['error' => 'Insufficient points'], 400);
            }

            // Deduct points from customer
            $customer->points -= $totalPrice;
            $customer->save();

            // Create order
            $order = new Order();
            $order->user_id = $customer->id;
            $order->book_id = $request->input('book_id');
            $order->quantity = $request->input('quantity');
            $order->save();

            return response()->json(['message' => 'Order placed successfully']);
        }
        public function cancel($orderId)
        {
            $order = Order::find($orderId);

            if (!$order) {
                return response()->json(['error' => 'Order not found'], 404);
            }

            // Refund points to customer
            $customer = $order->user;
            $customer->points += ($order->book->price * $order->quantity);
            $customer->save();

            // Delete the order
            $order->delete();

            return response()->json(['message' => 'Order canceled successfully']);
        }


}
