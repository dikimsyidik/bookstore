<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'writer', 'cover_image', 'price'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'book_tags');
    }
}
