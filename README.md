Getting Started

Install PHP dependencies using Composer:
composer install

Copy the .env.example file to .env:
cp .env.example .env

Generate the application key:
php artisan key:generate

Configure your database
connection in the .env file.

Run database migrations:
php artisan migrate

install passport key:
php artisan passport:install

Serve the application:
php artisan serve
Access the application in your web browser at http://127.0.0.1:8000.
Access the DOCS API in your web browser at http://127.0.0.1:8000/request-docs.

API docs is in
