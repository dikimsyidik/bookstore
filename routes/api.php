<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\UserAuthContreoller;
use App\Http\Controllers\OrderController;
use OpenApi\Annotations as OA;
use Illuminate\Support\Facades\Auth;



Route::prefix('books')->group(function () {
    Route::get('/', [BookController::class, 'index']);
    Route::post('/', [BookController::class, 'store']);
    Route::put('/{book_id}', [BookController::class, 'update']);
    Route::delete('/{book_id}', [BookController::class, 'destroy']);
});

Route::prefix('order')->middleware('auth:api')->group(function () {
    Route::get('/', [OrderController::class, 'index']);
    Route::post('/', [OrderController::class, 'order']);
    Route::delete('/{order_id}', [OrderController::class, 'cancel']);
});

Route::prefix('auth')->group(function () {
    Route::post('login', [UserAuthContreoller::class, 'login']);
    Route::post('register', [UserAuthContreoller::class, 'register']);
});

