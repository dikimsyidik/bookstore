<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Book;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $books = [
            [
                'title' => 'To Kill a Mockingbird',
                'writer' => 'Harper Lee',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 12.99,
                'tags' => ['fiction', 'classic'],

            ],
            [
                'title' => '1984',
                'writer' => 'George Orwell',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 10.99,
                'tags' => ['science', 'classic'],

            ],
            [
                'title' => 'The Catcher in the Rye',
                'writer' => 'J.D. Salinger',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 9.99,
                'tags' => ['eassy'],

            ],
            [
                'title' => 'The Great Gatsby',
                'writer' => 'F. Scott Fitzgerald',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 11.99,
            ],
            [
                'title' => 'To the Lighthouse',
                'writer' => 'Virginia Woolf',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 13.99,
            ],
            [
                'title' => 'Brave New World',
                'writer' => 'Aldous Huxley',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 14.99,
            ],
            [
                'title' => 'The Hobbit',
                'writer' => 'J.R.R. Tolkien',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 15.99,
            ],
            [
                'title' => 'Pride and Prejudice',
                'writer' => 'Jane Austen',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 8.99,
            ],
            [
                'title' => 'Crime and Punishment',
                'writer' => 'Fyodor Dostoevsky',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 16.99,
            ],
            [
                'title' => 'Moby-Dick',
                'writer' => 'Herman Melville',
                'cover_image' => 'https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg',
                'price' => 17.99,
            ],
        ];

        Book::truncate();
        DB::table('book_tags')->truncate();
         foreach ($books as $bookData) {
            $tags = $bookData['tags'] ?? [];
            unset($bookData['tags']);
            $book = Book::create($bookData);

            // Associate tags with the book
            foreach ($tags as $tagName) {
                // Find or create the tag
                $tag = Tag::firstOrCreate(['name' => $tagName]);

                // Attach the tag to the book
                $book->tags()->attach($tag);
            }
        }
    }
}
