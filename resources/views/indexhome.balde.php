<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bookstore</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <div id="app">
        <div class="container">
            <h1>Bookstore</h1>
            <div class="book-container">
                <div v-for="book in books" :key="book.id" class="book-card">
                    <div class="book-title">@{{ book.title }}</div>
                    <div class="book-writer">By: @{{ book.writer }}</div>
                    <img :src="book.cover_image" alt="Book Cover">
                    <div class="book-price">$@{{ book.price }}</div>
                    <div class="book-tags">Tags: @{{ book.tags.join(', ') }}</div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
