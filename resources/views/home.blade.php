<!-- resources/views/courses.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Courses</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
    <style>
        /* Add your custom styles here */
    </style>
</head>
<body>

<div class="container">
    <h1>Book Store</h1>
    <div class="row mb-4">
        <div class="col-md-6">
            <form id="search-form">
                <div class="form-group">
                    <input type="text" class="form-control" id="search-title" placeholder="Search by Title">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="search-writer" placeholder="Search by Writer">
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="button" class="btn btn-secondary" id="reset">Reset</button>
            </form>
        </div>
    </div>
    <div class="row" id="book-list">

    </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        let currentPage = 0;
        let lastPage = 1;
        let isLoading = false;

        function loadBooks(page = 1, title = '', writer = '') {
            if (isLoading || currentPage > lastPage) return;
            isLoading = true;
            $.ajax({
                url: '/api/books',
                type: 'GET',
                data: { page: page, title: title, writer: writer },
                success: function(response) {
                    lastPage = response.last_page
                    if (response.data.length > 0) {
                        $.each(response.data, function(index, book) {
                            let tagsHTML = '';
                            // Check if the book has tags
                            if (book.tags && book.tags.length > 0) {
                                // Iterate over each tag and create a tag HTML string
                                $.each(book.tags, function(tagIndex, tag) {
                                    tagsHTML += `<span class="badge badge-primary">${tag.name}</span> `;
                                });
                            }
                            $('#book-list').append(
                                `<div class="col-md-4 mb-4">
                                    <div class="card">
                                        <img src="${book.cover_image}" class="card-img-top" alt="Book Cover">
                                        <div class="card-body">
                                            <h5 class="card-title">${book.title}</h5>
                                            <p class="card-text">${book.writer}</p>
                                            <p class="card-text">$${book.price}</p>
                                            <div class="tags">${tagsHTML}</div>
                                        </div>
                                    </div>
                                </div>`
                            );
                        });
                        currentPage = response.current_page;
                        lastPage = response.last_page;
                    }
                    isLoading = false;
                },
                error: function(xhr, status, error) {
                    console.error('Error fetching books', error);
                    isLoading = false;
                }
            });
        }

        loadBooks();

        $('#search-form').submit(function(event) {
            event.preventDefault();
            let title = $('#search-title').val();
            let writer = $('#search-writer').val();
            if (title != '' || writer != '' ) {
                $('#book-list').empty();
                loadBooks(1, title, writer);
            }

        });

        $('#reset').click(function() {
            $('#search-title').val('');
            $('#search-writer').val('');
            $('#book-list').empty();
            loadBooks();
        });

        $(window).scroll(function() {
            if ($(window).scrollTop() + $(window).height() >= $(document).height() * 0.8) {
                loadBooks(currentPage + 1);
            }
        });
    });
</script>


</body>
</html>
