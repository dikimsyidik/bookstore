<div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-4">
    <img src="https://images-na.ssl-images-amazon.com/images/I/51Ga5GuElyL._AC_SX184_.jpg" alt="Book Cover" class="w-full h-auto">
    <h3 class="text-lg font-semibold mt-2">Example Book Title</h3>
    <p class="text-sm text-gray-600">Author: Example Author</p>
    <p class="text-sm text-gray-600">Price: $10.00</p>
    <div class="mt-2">
        <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-xs font-semibold text-gray-700 mr-2">Fiction</span>
        <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-xs font-semibold text-gray-700 mr-2">Science</span>
    </div>
    <button type="button" class="mt-3 py-2 px-4 bg-blue-500 text-white rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600" data-bs-toggle="modal" data-bs-target="#bookModal">
        View Details
    </button>
</div>
